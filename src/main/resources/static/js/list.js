$(document).ready( function () {
    $('#employeesTable').DataTable({
 			ajax: {
 				"url": '/api/employees',
                "data": function (d) {
                    var table = $('#employeesTable').DataTable()
                    d.page = (table != undefined) ? table.page.info().page : 0
                    d.size = (table != undefined) ? table.page.info().length : 5
                    d.sort = d.columns[d.order[0].column].data + ',' + d.order[0].dir
					d.search = (table != undefined) ? $('.dataTables_filter input').val() : ''
                }
			},
        	// "searching":false,
        	"processing": true,
        	"serverSide": true,
        	"lengthMenu": [[5, 10, 15, 30, 50, 75, 100], [5, 10, 15, 30, 50, 75, 100]],
			"aoColumns": [
			      { "mData": "id"},
		          { "mData": "name" },
				  { "mData": "lastName" },
				  { "mData": "email" },
				  { "mData": "phone" },
				  { "mData": "active" }
			],
			// "oLanguage": {
			// 	"sLengthMenu": "Show _MENU_ records", //change enties to records
             //    "sInfo": "Showing _START_ to _END_ of _TOTAL_ records"
			// }
	 });

    $('.dataTable').on('click', 'tbody tr', function() {
        console.log('API row values : ', table.row(this).data());
    })
});