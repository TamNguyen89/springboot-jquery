$(document).ready( function () {

    $("#create-form").submit(function (event) {

        //stop submit the form, we will post it manually.
        event.preventDefault();

        create();

    });
});

function create() {
    $('#feedback').html('');

    var dataForm = {}
    dataForm["name"] = $("#name").val();
    dataForm["lastName"] = $("#lastName").val();
    dataForm["email"] = $("#email").val();
    dataForm["phone"] = $("#phone").val();
    if ($('input[name=active]:checked', '#create-form').val() == 'active') {
        dataForm["active"] = true;
    } else {
        dataForm["active"] = false;
    }

    console.log(JSON.stringify(dataForm));

    $("#btn-create").prop("disabled", true);

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/api/employees",
        data: JSON.stringify(dataForm),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            var json = data.message;
            $('#feedback').html(json);

            console.log("SUCCESS : ", data);
            $("#btn-search").prop("disabled", false);

        },
        error: function (e) {

            // var json = "<h4>Ajax Response</h4><pre>"
            //     + e.responseText + "</pre>";
            // $('#feedback').html(json);

            console.log("ERROR : ", e);
            $("#btn-search").prop("disabled", false);

        }
    });

}