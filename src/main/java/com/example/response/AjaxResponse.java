package com.example.response;

public class AjaxResponse {

    private String message;

    public AjaxResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
