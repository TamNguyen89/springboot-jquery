package com.example.repository;

import com.example.entity.EmployeeEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface EmployeeRepository extends JpaRepository<EmployeeEntity, Long>{

    @Query("SELECT e FROM #{#entityName} e WHERE e.name LIKE %:searchQuery%")
    Page<EmployeeEntity> findBySearchQuery(@Param("searchQuery") String searchQuery, Pageable pageable);

}
