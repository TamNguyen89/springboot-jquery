package com.example.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping({"/", "/employees"})
public class EmployeeController {
	
	@GetMapping
	public String showEmployees(){
		return "list";
	}

	@GetMapping("/create")
	public String showCreate() {
		return "create";
	}
}
