package com.example.controller;

import java.util.HashMap;
import java.util.Map;

import com.example.request.CreateEmployeeRequest;
import com.example.response.AjaxResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.entity.EmployeeEntity;
import com.example.service.EmployeeService;

@RestController
@RequestMapping("/api/employees")
public class EmployeeRestController {
	
	@Autowired
	private EmployeeService employeeService;
	
	@GetMapping
	public Map<String, Object> getAllEmployees(Pageable pageable,
											   @RequestParam("draw") Integer draw ,
											   @RequestParam(value = "search", defaultValue = "") String search){
		Map<String, Object> map = new HashMap<>();
		Page page = employeeService.getAllEmployees(pageable, search);
		map.put("data",page.getContent());
		map.put("draw", draw);
		map.put("recordsTotal",page.getTotalElements());
		map.put("recordsFiltered",page.getTotalElements());
		return map;
	}

	@GetMapping("/{id}")
	public EmployeeEntity getEmployeeById(@PathVariable("id") long id){
		return employeeService.getEmployeeById(id);
	}

	@PostMapping
	public ResponseEntity createEmployee(@RequestBody CreateEmployeeRequest request) {
		employeeService.createEmployee(request);
		return ResponseEntity.ok(new AjaxResponse("Create customer successfully!"));
	}



}
