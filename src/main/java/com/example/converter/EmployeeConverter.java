package com.example.converter;

import com.example.entity.EmployeeEntity;
import com.example.request.CreateEmployeeRequest;
import com.example.response.EmployeeResponse;

public class EmployeeConverter {

    public static EmployeeEntity convertToEntity(CreateEmployeeRequest request) {
        EmployeeEntity employeeEntity = new EmployeeEntity();
        employeeEntity.setName(request.getName());
        employeeEntity.setLastName(request.getLastName());
        employeeEntity.setEmail(request.getEmail());
        employeeEntity.setPhone(request.getPhone());
        request.getActive();
        employeeEntity.setActive(request.getActive());
        return employeeEntity;
    }

    public static EmployeeResponse convertToResponse(EmployeeEntity entity) {
        EmployeeResponse response = new EmployeeResponse();
        response.setId(entity.getId());
        response.setName(entity.getName());
        response.setLastName(entity.getLastName());
        response.setEmail(entity.getEmail());
        response.setPhone(entity.getPhone());
        response.setActive(entity.isActive());
        return response;
    }
}
