package com.example.service;

import java.util.List;
import java.util.stream.Collectors;

import com.example.converter.EmployeeConverter;
import com.example.request.CreateEmployeeRequest;
import com.example.response.EmployeeResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.entity.EmployeeEntity;
import com.example.repository.EmployeeRepository;
import org.springframework.util.StringUtils;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;

	@Override
	public Page<EmployeeResponse> getAllEmployees(Pageable pageable, String search) {
		if (StringUtils.isEmpty(search)) {
			return employeeRepository.findAll(pageable).map(EmployeeConverter::convertToResponse);
		}
		return employeeRepository.findBySearchQuery(search, pageable).map(EmployeeConverter::convertToResponse);

	}

	@Override
	public EmployeeEntity getEmployeeById(long id) {
		return employeeRepository.findOne(id);
	}

	@Override
	public void createEmployee(CreateEmployeeRequest request) {
		employeeRepository.save(EmployeeConverter.convertToEntity(request));
	}

}
