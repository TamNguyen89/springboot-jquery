package com.example.service;

import java.util.List;

import com.example.entity.EmployeeEntity;
import com.example.request.CreateEmployeeRequest;
import com.example.response.EmployeeResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public interface EmployeeService {
	
	Page<EmployeeResponse> getAllEmployees(Pageable pageRequest, String search);
	EmployeeEntity getEmployeeById(long id);
	void createEmployee(CreateEmployeeRequest request);
	
}
